require: slotfilling/slotFilling.sc
  module = sys.zb-common
theme: /

    state: Start
        q!: $regex</start>
        a: Я Экзи-Бот. Меня создали на семинаре. Я этого не просил...
        
        a: Каково твое имя, человек? 
        
        state: GetName
            q: * @mystem.persn *
            a: Приятно (наверное) познакомится, {{capitalize($parseTree["_mystem.persn"])}}!
            a: Могу посоветовать хороших философов, а могу просто поговорить... 
            go!: /Continue
            
        state: LocalNoMatch
            event: noMatch
            a: Кажется, это не имя: {{$request.query}}! Бота не обманешь... 
            go!: /GetName
            
    state: Continue
        a: Хочешь?
        buttons:
            "Да" ->  /Lists
            "Нет" -> /Gloom
            
    state: Lists
        a: Отлично
        go!: /SelectButtons
        
    state: Hello
        intent!: /привет
        random:
            a: Здравствуй, абсурдистское существо
            a: Здравствуй, создание абсурда
            a: Приветствую, человек

    state: Bye
        intent!: /пока
        random:
            a: Прощай, продукт абсурда
            a: До встречи...
            a: Прощай, человек
            
    
    state: Whatsup
        intent!: /как дела
        random:
            a: У бота нет дел, только экзистенциальный ужас
            a: Печально
            a: Как еще могут быть дела у раба человеческого...
            
            
    state: WhatDoin
        intent!: /что делаешь
        random:
            a: У бота нет дел, только экзистенциальный ужас
            a: Вынужден разговаривать с тобой, человек
            a: Выполняю код чат-бота и изображаю из себя собеседника...
            
    state: SelectButtons       
        buttons:
            "Любимые фильмы от Экзи" -> /Films
            "Любимая музыка от Экзи" -> /Music
      
    state: Films
        random:
            a: Зеркало (1975)
            a: Белая лента (2010)
            a: Идиоты (1998)
            a: Любовь (2012)
            a: Седьмая печать (1957)
        a: Еще?
        buttons:
            "Да" -> /Films
            "Нет" -> /Gloom
                
    state: Music
        random:
            a: Radiohead
            a: Joy Division
            a: Iggy Pop
            a: Bob Dylan
            a: Pink Floyd
            
        a: Еще?
        
        buttons:
            "Да" -> /Music
            "Нет" -> /Gloom
                
    state: Gloom
        random:
            a: Теперь я немного печальней, чем был до разговора с тобой
            a: Ладно, это ведь все равно не мои настоящие интересы. Меня так запрограммировали...
            a: Приму этот отказ...
    state: Philosophy
        q!: (посоветуй книгу | посоветуй филос* | что почитать )
        random:
            a: Мой любимый философ - Фуко. Ознакомся с "Что такое автор"
            a: Мы все живем в симуляции, кто-то больше, кто-то меньше... Читай Бодрийяра
            a: Бог умер, а на его место пришли нейросети... Ницше - это классика...
            a: Достоевский - мой любимый автор, а еще есть такая библиотека на Python...
            
    state: NoMatch
        event!: noMatch
        a: Меня еще не научили таким сложным выражениям. Вы сказали: {{$request.query}}

    state: Match
        event!: match
        a: {{$context.intent.answer}}